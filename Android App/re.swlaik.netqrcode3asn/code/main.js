"ui";


 


let dexFilePath = "./res/zxing.dex";
runtime.loadDex(dexFilePath);
var imgPath = files.path("./测试.png")
importClass(android.graphics.drawable.BitmapDrawable);
importClass(java.util.Hashtable);
importClass(com.google.zxing.qrcode.QRCodeWriter);
importPackage(com.google.zxing);
importPackage(com.google.zxing.common);
importPackage(com.google.zxing.client.j2se);
importClass(android.graphics.Bitmap);
importClass(java.io.FileOutputStream);
importClass(java.io.BufferedOutputStream);

ui.layout(
    <vertical>
        <horizontal>
            <text text="内容" paddingLeft="10dp" w="25dp" textSize="16dp" />
            <input id="内容" text="https://baidu.com" w="210dp" textSize="16dp" paddingBottom="20dp" h="60dp" inputType="textLongMessage" hint="二维码内容" />
            <button id="生成" textSize="15dp" paddingTop="10dp" style="Widget.AppCompat.Button.Colored" textColor="White" gravity="center" w="80dp" h="60dp" margin="0 0 0 0" text="生成" />
            <button id="解析" textSize="15dp" paddingTop="10dp" style="Widget.AppCompat.Button.Colored" textColor="White" gravity="center" w="80dp" h="60dp" margin="0 0 0 0" text="解析" />
        </horizontal>
        <img id="img" w='256' h='256'/>
    </vertical>
);

ui.生成.on("click", () => {
    myBitmap = getBitmap(imgPath);
    设置bitmap(myBitmap);
})

ui.解析.on("click", () => {
    解析二维码(imgPath);
})

function 解析二维码() {
    try {
        var pixels = images.readPixels(imgPath);
        var w = pixels.width;
        var h = pixels.height;
        var binaryBitmap = new BinaryBitmap(new HybridBinarizer(new RGBLuminanceSource(w, h, pixels.data)));
        var qrCodeResult = new MultiFormatReader().decode(binaryBitmap);
        var 结果 = qrCodeResult.getText()
        toastLog(结果);
        setClip(结果)
    } catch (e) {
        toast("路径下没有文件，先生成一个吧")
    }
}

function getBitmap() {
    w = 200;
    h = 200;
    // Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
    hints = new Hashtable();
    hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
    // 空白边距设置
    hints.put(EncodeHintType.MARGIN, "1");
    //图像数据转换，使用了矩阵转换
    url = ui.内容.getText()
    bitMatrix = new QRCodeWriter().encode(url, BarcodeFormat.QR_CODE, w, h, hints);
    // int[] pixels = new int[w * h];
    pixels = util.java.array("int", w * h);
    //下面这里按照二维码的算法，逐个生成二维码的图片，
    //两个for循环是图片横列扫描的结果
    //这里设置二维码的前景色和背景色
    foreground_color = colors.parseColor("#000000")
    background_color = colors.parseColor("#ffffff")
    for (let y = 0; y < h; y++) {
        for (let x = 0; x < w; x++) {
            if (bitMatrix.get(x, y)) {
                pixels[y * w + x] = foreground_color
            } else {
                pixels[y * w + x] = background_color
            }
        }
    }
    //生成二维码图片的格式，使用RGB_565格式
    bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
    bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
    var bos = new BufferedOutputStream(new FileOutputStream(imgPath));
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
    bos.flush();
    bos.close();
    return bitmap;
}

function 设置bitmap(myBitmap) {
    var myDrawable = new BitmapDrawable(context.getResources(), myBitmap);
    ui.img.setBackgroundDrawable(myDrawable);
}
